Chapter 3: Objects
------------------

The sympol types are numbers, strings, and booleans. In addition, there
are two special values <code>null</code> and <code>undefined</code>. God
knows what "type" they are.

All other values are *objects*. (Numbers, strings, and booleans are 
object-like in that they have methods, but they are immutable). Objects
in JavaScript are mutable keyed collections. (Objects are property bags).

So even functions are objects.
